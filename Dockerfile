FROM golang:latest
RUN mkdir /app
COPY . /app/
RUN /app/build.sh
CMD ["/app/run/bin/service.sh", "start"]