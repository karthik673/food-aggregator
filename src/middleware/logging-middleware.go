package middleware

import (
	"context"
	"time"

	"food.aggregator/model"
	"food.aggregator/service"
	"github.com/go-kit/kit/log"
)

type Middleware func(service.Service) service.Service

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next service.Service) service.Service {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   service.Service
	logger log.Logger
}

func (mw loggingMiddleware) BuyItem(ctx context.Context, requestBody model.BuyItemReqBody) (model.BuyItemRespBody, error) {

	defer func(begin time.Time) {
		mw.logger.Log("api", "BuyItem", "took", time.Since(begin))
	}(time.Now())
	mw.logger.Log("api", "BuyItem", "Start", time.Now())
	return mw.next.BuyItem(ctx, requestBody)
}

func (mw loggingMiddleware) BuyItemQty(ctx context.Context, requestBody model.BuyItemQtyReqBody) (model.BuyItemQtyRespBody, error) {
	defer func(begin time.Time) {
		mw.logger.Log("api", "BuyItemQty", "took", time.Since(begin))
	}(time.Now())
	mw.logger.Log("api", "BuyItemQty", "Start", time.Now())
	return mw.next.BuyItemQty(ctx, requestBody)
}

func (mw loggingMiddleware) BuyItemQtyPrice(ctx context.Context, requestBody model.BuyItemQtyPriceReqBody) (model.BuyItemQtyPriceRespBody, error) {
	defer func(begin time.Time) {
		mw.logger.Log("api", "BuyItemQtyPrice", "took", time.Since(begin))
	}(time.Now())
	mw.logger.Log("api", "BuyItemQtyPrice", "Start", time.Now())
	return mw.next.BuyItemQtyPrice(ctx, requestBody)
}

func (mw loggingMiddleware) ShowSummary(ctx context.Context) ([]model.ShowSummaryRespBody, error) {
	defer func(begin time.Time) {
		mw.logger.Log("api", "ShowSummary", "took", time.Since(begin))
	}(time.Now())
	mw.logger.Log("api", "ShowSummary", "Start", time.Now())
	return mw.next.ShowSummary(ctx)
}

func (mw loggingMiddleware) FastBuyItem(ctx context.Context, requestBody model.FastBuyItemReqBody) (model.FastBuyItemRespBody, error) {
	defer func(begin time.Time) {
		mw.logger.Log("api", "FastBuyItem", "took", time.Since(begin))
	}(time.Now())
	mw.logger.Log("api", "FastBuyItem", "Start", time.Now())
	return mw.next.FastBuyItem(ctx, requestBody)
}
