package falist

type ListItem struct {
	Name     string
	Quantity int
	Price    float64
}

type node struct {
	Item ListItem
	next *node
}

type FaList struct {
	head *node
	len  int
}

func NewList() *FaList {
	return &FaList{}
}

func (list *FaList) Insert(itm ListItem) {
	n := node{}
	n.Item = itm
	if list.len == 0 {
		list.head = &n
		list.len++
		return
	}
	ptr := list.head
	for i := 0; i < list.len; i++ {
		if ptr.next == nil {
			ptr.next = &n
			list.len++
			return
		}
		ptr = ptr.next
	}
}

func (list *FaList) Search(name string) (int, ListItem) {
	ptr := list.head
	for i := 0; i < list.len; i++ {
		if ptr.Item.Name == name {
			return i, ptr.Item
		}
		ptr = ptr.next
	}
	return -1, ListItem{}
}

func (list *FaList) SearchAndInsert(item ListItem) {
	ptr := list.head
	for i := 0; i < list.len; i++ {
		if ptr.Item.Name == item.Name {
			ptr.Item.Price += item.Price
			ptr.Item.Quantity += item.Quantity
			return
		}
		ptr = ptr.next
	}
	list.Insert(item)
}

func (list *FaList) GetList() []ListItem {
	ptr := list.head
	l := []ListItem{}
	for i := 0; i < list.len; i++ {
		l = append(l, ptr.Item)
		ptr = ptr.next
	}
	return l
}
