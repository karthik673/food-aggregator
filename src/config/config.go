package config

import (
	"os"
	"time"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Address           string        `yaml:"address"`
	TimeOut           time.Duration `yaml:"timeout"`
	FruitSupplier     string        `yaml:"fruitSupplier"`
	VegetableSupplier string        `yaml:"vegetableSupplier"`
	GrainSupplier     string        `yaml:"grainSupplier"`
}

func NewConfig() *Config {
	return &Config{}
}

func (cfg *Config) InitCfg(filepath string) error {
	file, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer file.Close()
	err = yaml.NewDecoder(file).Decode(cfg)
	if err != nil {
		return err
	}
	return nil
}
