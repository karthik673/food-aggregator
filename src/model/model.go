package model

import "errors"

type BuyItemReqBody struct {
	Name string `json:"name"`
}
type BuyItemRespBody struct {
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity int    `json:"quantity"`
}
type BuyItemResp struct {
	Body  BuyItemRespBody
	Error error
}

type BuyItemQtyReqBody struct {
	Name     string `json:"name"`
	Quantity int    `json:"quantity"`
}
type BuyItemQtyRespBody struct {
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity int    `json:"quantity"`
}
type BuyItemQtyResp struct {
	Body  BuyItemQtyRespBody
	Error error
}

type BuyItemQtyPriceReqBody struct {
	Name     string `json:"name"`
	Quantity int    `json:"quantity"`
	Price    string `json:"price"`
}
type BuyItemQtyPriceRespBody struct {
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity int    `json:"quantity"`
}
type BuyItemQtyPriceResp struct {
	Body  BuyItemQtyPriceRespBody
	Error error
}

type ShowSummaryRespBody struct {
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity int    `json:"quantity"`
}
type ShowSummaryResp struct {
	Body  []ShowSummaryRespBody
	Error error
}

type FastBuyItemReqBody struct {
	Name string `json:"name"`
}
type FastBuyItemRespBody struct {
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity int    `json:"quantity"`
}
type FastBuyItemResp struct {
	Body  FastBuyItemRespBody
	Error error
}

var (
	ErrNotFound   = errors.New("item not found")
	ErrBadRequest = errors.New("bad request")
	ErrInternal   = errors.New("internal server error")
)
