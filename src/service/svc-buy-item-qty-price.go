package service

import (
	"context"
	"fmt"

	"food.aggregator/falist"
	"food.aggregator/model"
)

func (svc *FoodAggregatorService) BuyItemQtyPrice(ctx context.Context, requestBody model.BuyItemQtyPriceReqBody) (model.BuyItemQtyPriceRespBody, error) {
	respBody := model.BuyItemQtyPriceRespBody{}
	resp := falist.ListItem{Name: requestBody.Name}
	if len(requestBody.Name) == 0 || requestBody.Quantity <= 0 || getAmount(requestBody.Price) <= 0.00 {
		return respBody, model.ErrBadRequest
	}
	indx, fromList := svc.List.Search(requestBody.Name)
	if indx >= 0 && fromList.Quantity >= requestBody.Quantity {
		svc.Log.Log("Fetching record from Data List")
		respBody = model.BuyItemQtyPriceRespBody{Name: fromList.Name, Price: fmt.Sprintf("%.2f", fromList.Price), Quantity: fromList.Quantity}
	}
	items, err := svc.GetSupplies(svc.Cfg.FruitSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		svc.parseAndInsertSupplierResp(items, &resp)
	}
	items, err = svc.GetSupplies(svc.Cfg.VegetableSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		svc.parseAndInsertSupplierResp(items, &resp)
	}
	items, err = svc.GetSupplies(svc.Cfg.GrainSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		svc.parseAndInsertSupplierResp(items, &resp)
	}
	if resp.Quantity < requestBody.Quantity || resp.Price > getAmount(requestBody.Price) {
		return respBody, model.ErrNotFound
	}
	if respBody.Quantity > 0 && (respBody.Quantity < requestBody.Quantity || getAmount(respBody.Price) > getAmount(requestBody.Price)) {
		return respBody, nil
	}
	return model.BuyItemQtyPriceRespBody{Name: resp.Name, Price: fmt.Sprintf("%.2f", resp.Price), Quantity: resp.Quantity}, nil
}
