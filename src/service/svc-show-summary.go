package service

import (
	"context"
	"fmt"

	"food.aggregator/model"
)

func (svc *FoodAggregatorService) ShowSummary(ctx context.Context) ([]model.ShowSummaryRespBody, error) {
	resp := []model.ShowSummaryRespBody{}
	var err error = nil
	list := svc.List.GetList()
	for _, val := range list {
		resp = append(resp, model.ShowSummaryRespBody{Name: val.Name, Price: fmt.Sprintf("%.2f", val.Price), Quantity: val.Quantity})
	}
	return resp, err
}
