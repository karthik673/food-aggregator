package service

import (
	"context"
	"fmt"
	"sync"

	"food.aggregator/falist"
	"food.aggregator/model"
)

var wg sync.WaitGroup

func (svc *FoodAggregatorService) FastBuyItem(ctx context.Context, requestBody model.FastBuyItemReqBody) (model.FastBuyItemRespBody, error) {
	respBody := model.FastBuyItemRespBody{}
	resp := falist.ListItem{Name: requestBody.Name}
	if len(requestBody.Name) == 0 {
		return respBody, model.ErrBadRequest
	}
	var fruitItems, vegItems, grainItems, items []Item
	var err error
	wg.Add(3)
	go func() {
		fruitItems, err = svc.GetSupplies(svc.Cfg.FruitSupplier, svc.Cfg.TimeOut)
		if err != nil {
			svc.Log.Log("GetSupplies to Fruit Supplier Failed")
		}
		wg.Done()
	}()
	go func() {
		vegItems, err = svc.GetSupplies(svc.Cfg.VegetableSupplier, svc.Cfg.TimeOut)
		if err != nil {
			svc.Log.Log("GetSupplies to Fruit Supplier Failed")
		}
		wg.Done()
	}()
	go func() {
		grainItems, err = svc.GetSupplies(svc.Cfg.GrainSupplier, svc.Cfg.TimeOut)
		if err != nil {
			svc.Log.Log("GetSupplies to Fruit Supplier Failed")
		}
		wg.Done()
	}()
	wg.Wait()
	items = append(items, fruitItems...)
	items = append(items, vegItems...)
	items = append(items, grainItems...)
	parseSupplierResp(items, &resp)
	if resp.Quantity == 0 {
		return respBody, model.ErrNotFound
	}
	return model.FastBuyItemRespBody{Name: resp.Name, Price: fmt.Sprintf("%.2f", resp.Price), Quantity: resp.Quantity}, nil

}
