package service

import (
	"context"
	"fmt"

	"food.aggregator/falist"
	"food.aggregator/model"
)

func (svc *FoodAggregatorService) BuyItemQty(ctx context.Context, requestBody model.BuyItemQtyReqBody) (model.BuyItemQtyRespBody, error) {
	respBody := model.BuyItemQtyRespBody{}
	resp := falist.ListItem{Name: requestBody.Name}
	if len(requestBody.Name) == 0 || requestBody.Quantity <= 0 {
		return respBody, model.ErrBadRequest
	}
	items, err := svc.GetSupplies(svc.Cfg.FruitSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		parseSupplierResp(items, &resp)
	}
	items, err = svc.GetSupplies(svc.Cfg.VegetableSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		parseSupplierResp(items, &resp)
	}
	items, err = svc.GetSupplies(svc.Cfg.GrainSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		parseSupplierResp(items, &resp)
	}
	if resp.Quantity < requestBody.Quantity {
		return respBody, model.ErrNotFound
	}
	return model.BuyItemQtyRespBody{Name: resp.Name, Price: fmt.Sprintf("%.2f", resp.Price), Quantity: resp.Quantity}, nil
}
