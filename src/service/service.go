package service

import (
	"context"

	"food.aggregator/config"
	"food.aggregator/falist"
	"food.aggregator/model"
	"github.com/go-kit/kit/log"
)

type Service interface {
	BuyItem(ctx context.Context, requestBody model.BuyItemReqBody) (model.BuyItemRespBody, error)
	BuyItemQty(ctx context.Context, requestBody model.BuyItemQtyReqBody) (model.BuyItemQtyRespBody, error)
	BuyItemQtyPrice(ctx context.Context, requestBody model.BuyItemQtyPriceReqBody) (model.BuyItemQtyPriceRespBody, error)
	ShowSummary(ctx context.Context) ([]model.ShowSummaryRespBody, error)
	FastBuyItem(ctx context.Context, requestBody model.FastBuyItemReqBody) (model.FastBuyItemRespBody, error)
}

type FoodAggregatorService struct {
	Cfg  *config.Config
	Log  log.Logger
	List *falist.FaList
}

func NewFoodAggregatorService(cfg *config.Config, logger log.Logger, list *falist.FaList) Service {
	return &FoodAggregatorService{Cfg: cfg, Log: logger, List: list}
}
