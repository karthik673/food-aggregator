package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"food.aggregator/falist"
	"food.aggregator/model"
)

type Item struct {
	Id       string `json:"id"`
	Name     string `json:"name"`
	Price    string `json:"price"`
	Quantity int    `json:"quantity"`
}

// type respItem struct {
// 	name     string
// 	price    float64
// 	quantity int
// }

func (svc FoodAggregatorService) GetSupplies(url string, timeout time.Duration) ([]Item, error) {
	var items []Item
	client := &http.Client{Timeout: time.Second * timeout}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return items, err
	}
	resp, err := client.Do(req)
	if err != nil {
		svc.Log.Log("Request to supplier failed")
		return items, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			svc.Log.Log(err)
			return items, err
		}
		if err := json.Unmarshal(bodyBytes, &items); err != nil {
			svc.Log.Log("Supplier response decoding failed ", err.Error())
			return items, err
		}
	}
	return items, nil
}

func getAmount(inp string) float64 {
	indx := strings.Index(inp, "$")
	if indx != -1 || indx < len(inp) {
		inp = inp[indx+1:]
	}
	amt, _ := strconv.ParseFloat(inp, 32)
	return amt
}
func parseSupplierResp(items []Item, resp *falist.ListItem) {
	for _, itm := range items {
		if strings.EqualFold(itm.Name, resp.Name) {
			resp.Quantity += itm.Quantity
			resp.Price += getAmount(itm.Price)
		}
	}
}

func (svc *FoodAggregatorService) parseAndInsertSupplierResp(items []Item, resp *falist.ListItem) {
	for _, itm := range items {
		if strings.EqualFold(itm.Name, resp.Name) {
			resp.Quantity += itm.Quantity
			resp.Price += getAmount(itm.Price)
		}
		svc.List.SearchAndInsert(falist.ListItem{Name: itm.Name, Quantity: itm.Quantity, Price: getAmount(itm.Price)})
	}
}

func (svc *FoodAggregatorService) BuyItem(ctx context.Context, requestBody model.BuyItemReqBody) (model.BuyItemRespBody, error) {
	respBody := model.BuyItemRespBody{}
	resp := falist.ListItem{Name: requestBody.Name}
	if len(requestBody.Name) == 0 {
		return respBody, model.ErrBadRequest
	}
	items, err := svc.GetSupplies(svc.Cfg.FruitSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		parseSupplierResp(items, &resp)
	}
	items, err = svc.GetSupplies(svc.Cfg.VegetableSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		parseSupplierResp(items, &resp)
	}
	items, err = svc.GetSupplies(svc.Cfg.GrainSupplier, svc.Cfg.TimeOut)
	if err != nil {
		svc.Log.Log("GetSupplies to Fruit Supplier Failed")
	} else {
		parseSupplierResp(items, &resp)
	}
	if resp.Quantity == 0 {
		return respBody, model.ErrNotFound
	}
	return model.BuyItemRespBody{Name: resp.Name, Price: fmt.Sprintf("%.2f", resp.Price), Quantity: resp.Quantity}, nil
}
