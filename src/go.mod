module food.aggregator

go 1.16

require (
	github.com/go-kit/kit v0.11.0
	github.com/gorilla/mux v1.8.0
	gopkg.in/yaml.v2 v2.3.0
)
