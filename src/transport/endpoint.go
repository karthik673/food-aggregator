package transport

import (
	"context"

	"food.aggregator/model"
	"food.aggregator/service"
	"github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	BuyItemEndpoint         endpoint.Endpoint
	BuyItemQtyEndpoint      endpoint.Endpoint
	BuyItemQtyPriceEndpoint endpoint.Endpoint
	ShowSummaryEndpoint     endpoint.Endpoint
	FastBuyItemEndpoint     endpoint.Endpoint
}

func MakeServerEndpoints(svc service.Service) Endpoints {
	return Endpoints{
		BuyItemEndpoint:         makeBuyItemEndpoint(svc),
		BuyItemQtyEndpoint:      makeBuyItemQtyEndpoint(svc),
		BuyItemQtyPriceEndpoint: makeBuyItemQtyPriceEndpoint(svc),
		ShowSummaryEndpoint:     makeShowSummaryEndpoint(svc),
		FastBuyItemEndpoint:     makeFastBuyItemEndpoint(svc),
	}
}

func makeBuyItemEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(model.BuyItemReqBody)
		resp, er := svc.BuyItem(ctx, req)
		return model.BuyItemResp{Body: resp, Error: er}, nil
	}
}

func makeBuyItemQtyEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(model.BuyItemQtyReqBody)
		resp, er := svc.BuyItemQty(ctx, req)
		return model.BuyItemQtyResp{Body: resp, Error: er}, nil
	}
}
func makeBuyItemQtyPriceEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(model.BuyItemQtyPriceReqBody)
		resp, er := svc.BuyItemQtyPrice(ctx, req)
		return model.BuyItemQtyPriceResp{Body: resp, Error: er}, nil
	}
}
func makeShowSummaryEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		resp, er := svc.ShowSummary(ctx)
		return model.ShowSummaryResp{Body: resp, Error: er}, nil
	}
}
func makeFastBuyItemEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(model.FastBuyItemReqBody)
		resp, er := svc.FastBuyItem(ctx, req)
		return model.FastBuyItemResp{Body: resp, Error: er}, nil
	}
}
