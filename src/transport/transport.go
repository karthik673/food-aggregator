package transport

import (
	"context"
	"encoding/json"
	"net/http"

	"food.aggregator/model"
	"food.aggregator/service"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHTTPHandler(svc service.Service, logger log.Logger) http.Handler {
	r := mux.NewRouter()
	e := MakeServerEndpoints(svc)
	options := []httptransport.ServerOption{
		httptransport.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		httptransport.ServerErrorEncoder(encodeError),
	}

	r.Methods("POST").Path("/buy-item").Handler(httptransport.NewServer(
		e.BuyItemEndpoint,
		decodeBuyItemRequest,
		encodeBuyItemResponse,
		options...,
	))

	r.Methods("POST").Path("/buy-item-qty").Handler(httptransport.NewServer(
		e.BuyItemQtyEndpoint,
		decodeBuyItemQtyRequest,
		encodeBuyItemQtyResponse,
		options...,
	))

	r.Methods("POST").Path("/buy-item-qty-price").Handler(httptransport.NewServer(
		e.BuyItemQtyPriceEndpoint,
		decodeBuyItemQtyPriceRequest,
		encodeBuyItemQtyPriceResponse,
		options...,
	))

	r.Methods("GET").Path("/show-summary").Handler(httptransport.NewServer(
		e.ShowSummaryEndpoint,
		decodeShowSummaryRequest,
		encodeShowSummaryResponse,
		options...,
	))

	r.Methods("POST").Path("/fast-buy-item").Handler(httptransport.NewServer(
		e.FastBuyItemEndpoint,
		decodeFastBuyItemRequest,
		encodeFastBuyItemResponse,
		options...,
	))
	return r
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case model.ErrNotFound:
		return http.StatusNotFound
	case model.ErrBadRequest:
		return http.StatusBadRequest
	case model.ErrInternal:
		return http.StatusInternalServerError
	default:
		return http.StatusInternalServerError
	}
}

func decodeBuyItemRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req model.BuyItemReqBody
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}
func encodeBuyItemResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp, ok := response.(model.BuyItemResp)
	if ok && resp.Error != nil {
		encodeError(ctx, resp.Error, w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=ytf-8")
	return json.NewEncoder(w).Encode(resp.Body)
}

func decodeBuyItemQtyRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req model.BuyItemQtyReqBody
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}
func encodeBuyItemQtyResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp, ok := response.(model.BuyItemQtyResp)
	if ok && resp.Error != nil {
		encodeError(ctx, resp.Error, w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=ytf-8")
	return json.NewEncoder(w).Encode(resp.Body)
}

func decodeBuyItemQtyPriceRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req model.BuyItemQtyPriceReqBody
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}
func encodeBuyItemQtyPriceResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp, ok := response.(model.BuyItemQtyPriceResp)
	if ok && resp.Error != nil {
		encodeError(ctx, resp.Error, w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=ytf-8")
	return json.NewEncoder(w).Encode(resp.Body)
}

func decodeShowSummaryRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	return nil, nil
}
func encodeShowSummaryResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp, ok := response.(model.ShowSummaryResp)
	if ok && resp.Error != nil {
		encodeError(ctx, resp.Error, w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=ytf-8")
	return json.NewEncoder(w).Encode(resp.Body)
}

func decodeFastBuyItemRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req model.FastBuyItemReqBody
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}
func encodeFastBuyItemResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp, ok := response.(model.FastBuyItemResp)
	if ok && resp.Error != nil {
		encodeError(ctx, resp.Error, w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=ytf-8")
	return json.NewEncoder(w).Encode(resp.Body)
}
