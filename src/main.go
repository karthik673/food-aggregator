package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"

	"food.aggregator/config"
	"food.aggregator/falist"
	"food.aggregator/middleware"
	"food.aggregator/service"
	"food.aggregator/transport"
	"github.com/go-kit/kit/log"
)

func main() {

	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.With(logger, "time", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	cfgFile := flag.String("conf", "", "Configuration File")
	flag.Parse()
	if len(*cfgFile) == 0 {
		logger.Log("Conf file name not received")
		os.Exit(1)
	}
	cfg := config.NewConfig()
	if err := cfg.InitCfg(*cfgFile); err != nil {
		logger.Log("Conf init failed with error: ", err.Error())
		os.Exit(1)
	}
	newList := falist.NewList()
	var svc service.Service
	{
		svc = service.NewFoodAggregatorService(cfg, logger, newList)
		svc = middleware.LoggingMiddleware(logger)(svc)
	}

	var httpHandler http.Handler
	{
		httpHandler = transport.MakeHTTPHandler(svc, log.With(logger, "component", "HTTP"))
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	url, err := url.Parse(cfg.Address)
	if err != nil {
		logger.Log("Url Parse Failed")
		os.Exit(1)
	}
	host, port, _ := net.SplitHostPort(url.Host)
	if host == "0.0.0.0" {
		host = ""
	}
	httpAddr := fmt.Sprintf("%s:%s", host, port)
	go func() {
		logger.Log("transport", "HTTP", "addr", httpAddr)
		errs <- http.ListenAndServe(httpAddr, httpHandler)
	}()

	logger.Log("exit", <-errs)
}
