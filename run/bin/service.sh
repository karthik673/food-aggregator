#!/bin/bash

BIN_NAME=food-aggregator

cd "$(dirname "$0")"/..

_start()
{
    bin/${BIN_NAME} --conf cfg/config.yaml
    wait
}

if [ "$1" == "start" ]; then
    _start
else
    echo "Usage: service.sh start"
    exit 1
fi