#!/bin/bash

cd "$(dirname "$0")/src"
EXEC_NAME=food-aggregator
EXEC_FULL_NAME=../run/bin/${EXEC_NAME}

rm -rf ${EXEC_FULL_NAME}

echo "Building Food Aggregator Service"

GOOS=linux go build -o ${EXEC_FULL_NAME} main.go
if test $? -eq 0
then
    echo "Build Success"
    exit 0
else
    echo "Build Failed"
    exit 1
fi